package main

import (
	"fmt"
	"os"
	"strings"
	"goacc/configuration"
	"goacc/core/utils"
	"goacc/core/entities"
)

type Command int

const (
	CommandNone    Command = iota + 1
	CommandRun
	CommandMigrate
)

var _entities = []interface{}{
	&entities.Account{},
	&entities.Transaction{},
	&entities.Currency{},
	&entities.TransactionTemplate{},
	// add entity here
}

func main() {
	err := configuration.LoadConfiguration("./config.yml")
	if err != nil {
		fmt.Println(err)
	}

	switch readArgs() {
	case CommandMigrate:
		utils.Migrate(configuration.GetConfiguration().Database, _entities, false)
	case CommandRun:
		Start(configuration.GetConfiguration())
	}
}

func readArgs() Command {
	if len(os.Args) > 0 {
		for _, v := range os.Args {
			if strings.Compare(v, "run") == 0 {
				return CommandRun
			}
			if strings.Compare(v, "migrate") == 0 {
				return CommandMigrate
			}
		}
	}
	return CommandNone
}
