package services

import (
	"time"
	"goacc/core/datasource"
	"goacc/core/utils"
	"goacc/core/entities"
)

func GetTransactions(startDate, endDate time.Time, sourceId uint) []*entities.Transaction {
	db := datasource.GetConnection()

	_transactions := []*entities.Transaction{}
	{
	}

	if err := db.Select(&_transactions).Where(&entities.Transaction{SourceID: sourceId}).Error; err != nil {
		panic(err)
	}

	transactions := []*entities.Transaction{}

	for _, t := range _transactions {
		if (t.TransactionDate.Before(endDate) && t.TransactionDate.After(startDate)) ||
			(t.TransactionDate == startDate || t.TransactionDate == endDate) {

			transactions = append(transactions, t)
		}
	}
	return transactions
}

func GetAllTransactions(startDate, endDate time.Time) []*entities.Transaction {
	db := datasource.GetConnection()

	_transactions := []*entities.Transaction{}
	{
	}

	if err := db.Select(&_transactions).Error; err != nil {
		panic(err)
	}

	transactions := []*entities.Transaction{}

	for _, t := range _transactions {
		if (t.TransactionDate.Before(endDate) && t.TransactionDate.After(startDate)) ||
			(t.TransactionDate == startDate || t.TransactionDate == endDate) {

			transactions = append(transactions, t)
		}
	}
	return transactions
}

func GetTransactionById(id uint) *entities.Transaction {
	transaction := &entities.Transaction{}
	transaction.ID = id

	db := datasource.GetConnection()
	if db.First(transaction).Error != nil {
		return nil
	}

	return transaction
}

func CreateNewTransaction(date time.Time, amount float64, description string, source *entities.Account, target *entities.Account) *entities.Transaction {
	if amount < 0 {
		panic("amount is negative")
	}

	transaction := &entities.Transaction{
		TransactionDate: utils.TruncateDate(date),
		Amount:          amount,
		Description:     description,
		Source:          source,
		Target:          target,
	}

	transaction.Save()

	return transaction
}