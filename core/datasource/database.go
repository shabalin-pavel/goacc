package datasource

import (
	"github.com/jinzhu/gorm"
	"net/url"
	"strconv"

	_ "database/sql"
	_ "github.com/lib/pq"
)


type DatabaseConfiguration struct {
	User           string `required:"true"`
	Password       string `required:"true"`
	Protocol       string `required:"true"`
	Host           string `required:"true"`
	Port           int    `required:"true"`
	DataBase       string `required:"true"`
	Schema         string `required:"true"`
	ConnectionType string `required:"true"`
}

func (config *DatabaseConfiguration) GetConnectionURL() (*url.URL) {
	confUrl := &url.URL{
		Host:   config.Host + ":" + strconv.Itoa(config.Port),
		Scheme: config.Protocol,
	}
	if config.User != "" && config.Password != "" {
		confUrl.User = url.UserPassword(config.User, config.Password)
	}
	confUrl.Path = config.DataBase
	return confUrl
}

var db *gorm.DB

func InitDB(dbconfig *DatabaseConfiguration) error {
	var err error;
	db, err = gorm.Open("postgres", dbconfig.GetConnectionURL().String()+"?sslmode=disable")
	db.LogMode(true)
	if err != nil {
		return err
	}
	return nil
}

func GetConnection() *gorm.DB {
	return db
}