//usr/bin/env go run $0 "$@"; exit
package utils

import (
	"fmt"
	"goacc/core/datasource"
)

func Migrate(dbconfig *datasource.DatabaseConfiguration, entities []interface{}, drop bool) {

	datasource.InitDB(dbconfig)

	fmt.Println("Connect with: " + dbconfig.GetConnectionURL().String())
	db := datasource.GetConnection()

	if drop {
		if err := db.DropTableIfExists(entities...).Error; err != nil {
			fmt.Println(err)
		}
	}
	if err := db.AutoMigrate(entities...).Error; err != nil {
		fmt.Println(err)
	}
}


func MigrateTest(entities []interface{})  {
	config := &datasource.DatabaseConfiguration{}

	config.Host = "localhost"
	config.Port = 5432
	config.ConnectionType = "TCP"
	config.DataBase = "goaccbase"
	//config.DataBase = "goaccbase_test"
	config.Schema = "main"
	config.Protocol = "postgres"
	config.Password = "goacc123"
	config.User = "goacc"

	Migrate(config, entities, true)
}
