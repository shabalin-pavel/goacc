package utils

import (
	"time"
	"fmt"
)

var TOTAL_DATE = time.Date(1990, 1, 1, 0, 0, 0, 0, time.Now().Location())

func TruncateDate(date time.Time) time.Time {
	return time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location())
}

func DurationDay(daysNumber int) time.Duration {
	duration, _ := time.ParseDuration(fmt.Sprintf("%dh", 24*daysNumber))
	return duration
}