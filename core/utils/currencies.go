package utils

import (
	"os"
	"fmt"
	"encoding/xml"
	"io/ioutil"
)

type CurrencyISO struct {
	XMLName    xml.Name `xml:"CcyNtry"`
	Country    string   `xml:"CtryNm"`
	Name       string   `xml:"CcyNm"`
	Code       string   `xml:"Ccy"`
	NumberCode uint     `xml:"CcyNbr"`
	Units      string   `xml:"CcyMnrUnts"`
}

type CcyTbl struct {
	XMLName    xml.Name       `xml:"CcyTbl"`
	Currencies []*CurrencyISO `xml:"CcyNtry"`
}

func LoadIsoCurrencies(fileName string) []*CurrencyISO {
	xmlFile, err := os.Open(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil
	}
	defer xmlFile.Close()

	table := CcyTbl{
		Currencies: []*CurrencyISO{},
	}

	byteValue, err := ioutil.ReadAll(xmlFile)

	if err != nil {
		fmt.Println("Error read file:", err)
	}
	fmt.Printf("Readed %d bytes\n", len(byteValue))

	if err := xml.Unmarshal(byteValue, &table); err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Loaded %d currencies.\n", len(table.Currencies))

	return table.Currencies
}
