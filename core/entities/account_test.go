package entities

import (
	"testing"
	"time"
	"goacc/core/utils"
)

func TestCreateNewAccount(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("TEST Account", 100.0, &Currency{Name: "test"})
	if account.ID < 1 {
		t.Fail()
	}
}

func TestGetAccountById(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Second Account", 0.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}
	_acc := GetAccountById(account.ID)

	if _acc.AccountName != account.AccountName {
		t.Fail()
	}

	if _acc.Currency == nil || _acc.Currency.Name != "USD" {
		t.Errorf("Currency is %v", _acc.Currency)
	}
}

func TestAccount_GetTransactions(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Account with TR", 0.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}

	CreateNewTransaction(time.Now(), 200.0, "transaction 1", account, nil)
	CreateNewTransaction(time.Now(), 200.0, "transaction 2", nil, account)
	CreateNewTransaction(time.Now(), 200.0, "transaction 3", account, nil)
	trs := account.GetTransactions()
	if len(trs) < 3 {
		t.Errorf("transactions number %d ", len(trs))
	}
}

func TestAccount_GetTransactionsWithPeriod(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Account with TR", 0.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}

	CreateNewTransaction(time.Now().Add(- time.Hour*24), 200.0, "transaction 1", account, nil)
	CreateNewTransaction(time.Now(), 200.0, "transaction 2", nil, account)
	CreateNewTransaction(time.Now(), 200.0, "transaction 3", account, nil)

	today := utils.TruncateDate(time.Now())
	trs := account.GetTransactionsWithPeriod(today, today)

	if len(trs) < 2 {
		t.Errorf("transactions number %d ", len(trs))
	}
}

func TestAccount_GetAmount(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Account with TR", 50.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}

	// 50 + 50 = 100
	CreateNewTransaction(time.Now().Add(- time.Hour*100), 50.0, "transaction 1", nil, account)
	// 100 - 120 = -20
	CreateNewTransaction(time.Now().Add(- time.Hour*72), 120.0, "transaction 1", account, nil)
	// -20 + 200 = 180
	CreateNewTransaction(time.Now().Add(- time.Hour*48), 200.0, "transaction 1", nil, account)
	// 180 - 100 = 80
	CreateNewTransaction(time.Now().Add(- time.Hour*24), 100.0, "transaction 1", account, nil)
	// 80 + 200 = 280
	CreateNewTransaction(time.Now(), 200.0, "transaction 2", nil, account)
	// 280 - 280 = 0.0
	CreateNewTransaction(time.Now(), 280.0, "transaction 3", account, nil)

	amount := account.GetAmount()
	if amount != 0.0 {
		t.Errorf("amount: %f", amount)
	}
}

func TestAccount_GetTotalAmountForDate(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Account with TR", 50.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}

	// 50 + 50 = 100
	CreateNewTransaction(time.Now().Add(- time.Hour*100), 50.0, "transaction 1", nil, account)
	// 100 - 120 = -20
	CreateNewTransaction(time.Now().Add(- time.Hour*72), 120.0, "transaction 1", account, nil)
	// -20 + 200 = 180
	CreateNewTransaction(time.Now().Add(- time.Hour*48), 200.0, "transaction 1", nil, account)
	// 180 - 100 = 80
	CreateNewTransaction(time.Now().Add(- time.Hour*24), 100.0, "transaction 1", account, nil)
	// 80 + 200 = 280
	CreateNewTransaction(time.Now(), 200.0, "transaction 2", nil, account)
	// 280 - 280 = 0.0
	CreateNewTransaction(time.Now(), 280.0, "transaction 3", account, nil)

	amount := account.GetTotalAmountForDate(time.Now().Add(- time.Hour * 24))
	if amount != 80.0 {
		t.Errorf("amount: %f", amount)
	}
}

func TestAccount_GetTransactionsAmountWithPeriod(t *testing.T) {
	InitializeTest()

	account := CreateNewAccount("Account with TR", 50.0, &Currency{Name: "USD"})
	if account.ID < 1 {
		t.Fail()
	}

	// 50 + 50 = 100
	CreateNewTransaction(time.Now().Add(- time.Hour*100), 50.0, "transaction 1", nil, account)
	// 100 - 120 = -20
	CreateNewTransaction(time.Now().Add(- time.Hour*72), 120.0, "transaction 1", account, nil)
	// -20 + 200 = 180
	CreateNewTransaction(time.Now().Add(- time.Hour*48), 200.0, "transaction 1", nil, account)
	// 180 - 100 = 80
	CreateNewTransaction(time.Now().Add(- time.Hour*24), 100.0, "transaction 1", account, nil)
	// 80 + 200 = 280
	CreateNewTransaction(time.Now(), 200.0, "transaction 2", nil, account)
	// 280 - 280 = 0.0
	CreateNewTransaction(time.Now(), 280.0, "transaction 3", account, nil)

	startA, endA := account.GetTransactionsAmountWithPeriod(time.Now().Add(- time.Hour*72), time.Now().Add(- time.Hour*24))
	if startA != -20.0 || endA != 100.0 {
		t.Errorf("start amount: %f; end amount: %f", startA, endA)
	}
}
