package entities

import (
	"github.com/jinzhu/gorm"
	"goacc/core/datasource"
	"time"
	"goacc/core/utils"
)

type Account struct {
	gorm.Model

	AccountName    string
	StarterBalance float64
	CurrencyID     uint
	Hidden         bool
	Currency       *Currency `gorm:"association_autoupdate:false"`
	Amount         float64   `gorm:"-"`
}

func (account *Account) Save() {
	db := datasource.GetConnection()
	db.Save(account)
}
func (account *Account) GetTransactions() []*Transaction {
	db := datasource.GetConnection()
	transactions := []*Transaction{}
	if db.Where(&Transaction{SourceID: account.ID}).Or(&Transaction{TargetID: account.ID}).Find(&transactions).Error != nil {
		return nil
	}

	return transactions
}

func (account *Account) GetTransactionsWithPeriod(start time.Time, end time.Time) []*Transaction {
	transactions := []*Transaction{}
	for _, transaction := range account.GetTransactions() {
		if transaction.TransactionDate.Before(end.Add(time.Minute)) &&
			transaction.TransactionDate.After(start.Add(-time.Minute)) {
			transactions = append(transactions, transaction)
		}
	}

	return transactions
}

func (account *Account) GetAmount() float64 {
	return account.calcAmount(account.GetTransactions()) + account.StarterBalance
}

func (account *Account) GetTransactionsAmountWithPeriod(start time.Time, end time.Time) (float64, float64) {
	return account.GetTotalAmountForDate(start), account.calcAmount(account.GetTransactionsWithPeriod(start, end))
}

func (account *Account) GetTotalAmountForDate(date time.Time) float64 {
	return account.calcAmount(account.GetTransactionsWithPeriod(utils.TOTAL_DATE, date)) + account.StarterBalance
}

func (account *Account) calcAmount(transactions []*Transaction) float64 {
	amount := float64(0)
	for _, transaction := range transactions {
		if transaction.SourceID == account.ID {
			amount -= transaction.Amount
		} else if transaction.TargetID == account.ID {
			amount += transaction.Amount
		}
	}

	return amount
}

func GetAccountById(id uint) *Account {
	db := datasource.GetConnection()
	account := &Account{}
	account.ID = id

	if db.First(account).Error != nil {
		return nil
	}

	currency := &Currency{}
	db.Model(account).Related(currency)
	account.Currency = currency
	account.Amount = account.GetAmount()

	return account
}

func GetAccountsIdList() []uint {
	db := datasource.GetConnection()
	accounts := []*Account{}
	ids := []uint{}

	db.Find(&accounts)
	for i := range accounts {
		ids = append(ids, accounts[i].ID)
	}

	return ids
}

func GetAccountsList() []*Account {
	db := datasource.GetConnection()
	accounts := []*Account{}

	db.Find(&accounts)
	for _, account := range accounts {
		account.Amount = account.GetAmount()
		currency := &Currency{}
		db.Model(account).Related(currency)
		account.Currency = currency
	}

	return accounts
}

func CreateNewAccount(name string, balance float64, currency *Currency) *Account {
	account := &Account{
		AccountName:    name,
		StarterBalance: balance,
		Currency:       currency,
	}
	account.Save()

	return account
}

func CreateAccount(account *Account) *Account {
	account.Save()
	return account
}

type Currency struct {
	NumberCode uint   `gorm:"primary_key"`
	Name       string `xml:"CcyNm"`
	Code       string `xml:"Ccy"`
}

func GetCurrenciesList() []*Currency {
	db := datasource.GetConnection()
	currencies := []*Currency{}
	db.Find(&currencies)

	return currencies
}
