package entities

import (
	"goacc/core/datasource"
	"goacc/core/utils"
)

func InitializeTest() {


	config := &datasource.DatabaseConfiguration{}
	//user: goacc
	//password: goacc123
	//protocol: "postgres"
	//host: "localhost"
	//port: 5432
	//database: "goaccbase"
	//schema: "main"
	//connectiontype: "TCP"
	config.Host = "localhost"
	config.Port = 5432
	config.ConnectionType = "TCP"
	config.DataBase = "goaccbase_test"
	config.Schema = "main"
	config.Protocol = "postgres"
	config.Password = "goacc123"
	config.User = "goacc"
	datasource.InitDB(config)

	entities := []interface{}{
		&Account{},
		&Transaction{},
		&Currency{},
		// add entity here
	}
	utils.MigrateTest(entities)
}
