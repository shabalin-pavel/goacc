package entities

import (
	"github.com/jinzhu/gorm"
	"time"
	"goacc/core/datasource"
)

type Transaction struct {
	gorm.Model

	Title            string
	TransactionDate  time.Time
	Amount           float64
	Confirmed        bool
	AutoConfirmation bool
	Description      string

	SourceID uint
	Source   *Account `gorm:"association_autoupdate:false"`

	TargetID uint
	Target   *Account `gorm:"association_autoupdate:false"`

	TemplateID *uint
	Template   *TransactionTemplate `gorm:"association_autoupdate:false"`
}

func (transaction *Transaction) Save() {
	db := datasource.GetConnection()
	db.Save(transaction)
}

func (transaction *Transaction) Delete() {
	db := datasource.GetConnection()
	db.Delete(transaction)
}
