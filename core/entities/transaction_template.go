package entities

import (
	"github.com/jinzhu/gorm"
	"time"
	"goacc/core/datasource"
	"goacc/core/utils"
)

type PostponingType uint
type StopConditionType uint
type PeriodType uint
type TemplateSaveMode uint

const (
	BeforeWeekEnd PostponingType = iota
	AfterWeekEnd
	BeforeSunday
	AfterSunday
	None

	TargetDate          StopConditionType = iota
	UntillNTransactions

	EveryMonth PeriodType = iota
	EveryWeek
	EveryNDays
	Once

	UpdateBefore        TemplateSaveMode = iota
	UpdateAfter
	UpdateTodayAndAfter
)

type TransactionTemplate struct {
	gorm.Model

	Title string

	StartDate        time.Time
	Postponing       PostponingType
	AutoConfirmation bool
	PeriodType       PeriodType
	Period           int

	SkipLastDay                  bool
	CorrectLastTransactionAmount bool

	Amount      float64
	Description string

	StopCondition StopConditionType
	EndDate       *time.Time
	TargetAmount  *float64
	TargetN       *int

	SourceID uint
	Source   *Account `gorm:"association_autoupdate:false"`

	TargetID uint
	Target   *Account `gorm:"association_autoupdate:false"`
}

func GetTemplates(sourceId uint) []*TransactionTemplate {
	db := datasource.GetConnection()
	_templates := []*TransactionTemplate{}
	{
	}

	if err := db.Select(&_templates).Where(&TransactionTemplate{SourceID: sourceId}).Error; err != nil {
		panic(err)
	}
	return _templates
}

func (tmp *TransactionTemplate) GetTransactions() []*Transaction {
	db := datasource.GetConnection()

	_transactions := []*Transaction{}
	{
	}

	_id := tmp.ID

	if err := db.Select(&_transactions).Where(&Transaction{TemplateID: &_id}).Error; err != nil {
		panic(err)
	}

	return _transactions
}

func (tmp *TransactionTemplate) SaveTemplate(mode TemplateSaveMode) {
	now := utils.TruncateDate(time.Now())
	oldTransactions := tmp.GetTransactions()
	for _, forDelete := range oldTransactions {
		if forDelete.TransactionDate.Before(now) && mode == UpdateBefore {
			forDelete.Delete()
		}
		if forDelete.TransactionDate.Equal(now) && mode == UpdateTodayAndAfter {
			forDelete.Delete()
		}
		if forDelete.TransactionDate.After(now) && (mode == UpdateTodayAndAfter || mode == UpdateAfter) {
			forDelete.Delete()
		}
	}

	transactions := tmp.GetTransactions()
	for _, transaction := range transactions {
		if transaction.TransactionDate.Before(now) && mode == UpdateBefore {
			transaction.Save()
		}
		if transaction.TransactionDate.Equal(now) && mode == UpdateTodayAndAfter {
			transaction.Save()
		}
		if transaction.TransactionDate.After(now) && (mode == UpdateTodayAndAfter || mode == UpdateAfter) {
			transaction.Save()
		}
	}
}

func (tmp *TransactionTemplate) generateTransactions() []*Transaction {
	_transactions := []*Transaction{}
	{
	}

	date := tmp.StartDate

	for tmp.checkEnd(date) {
		_transactions = append(_transactions, tmp.generateTransaction(date))
		date = tmp.generateNextDate(date)
	}
	return _transactions
}

func (tmp *TransactionTemplate) generateTransaction(date time.Time) *Transaction {
	return &Transaction{
		TransactionDate: date,
		Amount:          tmp.Amount,
		Confirmed:       false,
		Description:     tmp.Description,
		SourceID:        tmp.SourceID,
		TargetID:        tmp.TargetID,
		TemplateID:      &tmp.ID,
	}
}

func (tmp *TransactionTemplate) generateNextDate(date time.Time) time.Time {
	var next time.Time
	switch tmp.PeriodType {
	case EveryMonth:
		next = date.AddDate(0, 1, 0)
	case EveryNDays:
		next = date.AddDate(0, 0, tmp.Period)
	case EveryWeek:
		next = date.AddDate(0, 0, 7)
	}

	if tmp.Postponing == None {
		return next
	}

	switch next.Weekday() {
	case time.Sunday:
		if tmp.Postponing == BeforeSunday || tmp.Postponing == BeforeWeekEnd {
			next = next.AddDate(0, 0, -2)
		} else if tmp.Postponing == AfterSunday || tmp.Postponing == AfterWeekEnd {
			next = next.AddDate(0, 0, 1)
		}
	case time.Saturday:
		if tmp.Postponing == BeforeWeekEnd {
			next = next.AddDate(0, 0, -1)
		} else if tmp.Postponing == AfterWeekEnd {
			next = next.AddDate(0, 0, 2)
		}
	}
	return next
}

func (tmp *TransactionTemplate) checkEnd(date time.Time) bool {
	switch tmp.StopCondition {
	case TargetDate:
		if date.After(*tmp.EndDate) {
			if date.Equal(date) && !tmp.SkipLastDay {
				return true
			} else {
				return false
			}
		}
	}
	return true
}
