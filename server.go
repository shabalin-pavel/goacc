package main

import (
	"goacc/configuration"
	"github.com/gin-gonic/gin"
	"fmt"
	"goacc/core/datasource"
	"goacc/core/entities"
	"github.com/jinzhu/gorm"
	"goacc/core/utils"
	"goacc/api"
)

func Start(config configuration.Configuration) {
	datasource.InitDB(config.Database)

	initCurrencies(utils.LoadIsoCurrencies("./iso_currencies.xml"))

	engine := gin.Default()
	if !gin.IsDebugging() {
		engine.Static("/css", "./web/css")
		engine.Static("/js", "./web/js")
		engine.StaticFile("/", "./web/index.html")
	}

	engine.GET(apiRef("accounts_ids"), api.GetAccountsIdsHandler)
	engine.GET(apiRef("accounts"), api.GetAccountsListHandler)
	engine.GET(apiRef("account/:id"), api.GetAccountHandler)
	engine.POST(apiRef("account"), api.CreateNewAccount)

	engine.GET(apiRef("currencies"), api.GetCurrenciesList)

	engine.GET(apiRef("/transactions"), api.GetTransactions)
	engine.POST(apiRef("/transaction"), api.CreateTransaction)
	engine.GET(apiRef("/templates"), api.GetAllTemplates)

	engine.Run(fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port))
}

func apiRef(path string) string {
	return "/api/" + path
}

func initCurrencies(supportedCurrencies  []*utils.CurrencyISO) {

	db := datasource.GetConnection()

	for _, currency := range supportedCurrencies {
		if db.Find(&entities.Currency{}, &entities.Currency{NumberCode: currency.NumberCode}).Error == gorm.ErrRecordNotFound {
			db.Save(&entities.Currency{
				NumberCode:currency.NumberCode,
				Code:currency.Code,
				Name:currency.Name,
			})
		}
	}
}
