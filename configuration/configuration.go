package configuration

import (
	"github.com/jinzhu/configor"

	"goacc/core/datasource"
)

var _config Configuration

func LoadConfiguration(file string) error {
	_config.Database = &datasource.DatabaseConfiguration{}
	_config.Server = &ServerConfiguration{}
	_config.Application = &ApplicationConfiguration{}

	return configor.Load(&_config, "./config.yml")
}

func GetConfiguration() Configuration {
	return _config
}

type Configuration struct{
	Server      *ServerConfiguration
	Application *ApplicationConfiguration
	Database    *datasource.DatabaseConfiguration
}

type ApplicationConfiguration struct {
	Name string `default:"GreenWallDefaultApplication"`
}

type ServerConfiguration struct {
	Host         string `default:"localhost"`
	Port         string `default:"8080"`
	TLS          bool   `default:"false"`
	CertFileName string `default:""`
	KeyFileName  string `default:""`
}
