package api

import (
	"github.com/gin-gonic/gin"
	"goacc/core/entities"
	"strconv"
	"net/http"
	"fmt"
)

func GetAccountHandler(context *gin.Context) {
	id := context.Param("id")
	fmt.Printf("ID: %s", id)
	if _id, err := strconv.ParseInt(id, 10, 64); err == nil {
		account := entities.GetAccountById(uint(_id))
		fmt.Printf("ACCOUNT: %v", account)
		if account == nil {
			context.AbortWithStatus(http.StatusNotFound)
			return
		}
		context.JSON(http.StatusOK, account)
		return
	}
	context.AbortWithStatus(http.StatusBadRequest)
}

func GetAccountsIdsHandler(context *gin.Context) {
	context.JSON(http.StatusOK, entities.GetAccountsIdList())
}

func GetAccountsListHandler(context *gin.Context) {
	context.JSON(http.StatusOK, entities.GetAccountsList())
}

func CreateNewAccount(context *gin.Context) {
	account := &entities.Account{}
	err := context.BindJSON(account)
	if err != nil {
		context.Writer.Write([]byte(err.Error()))
		context.AbortWithStatus(http.StatusBadRequest)
		return
	}

	account = entities.CreateAccount(account)
	context.JSON(http.StatusOK, account)
}

func GetCurrenciesList(context *gin.Context) {
	context.JSON(http.StatusOK, entities.GetCurrenciesList())
}
