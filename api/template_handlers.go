package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"goacc/core/services"
)

func GetAllTemplates(ctx *gin.Context){
	ctx.JSON(http.StatusOK, services.GetAllTemplates())
}