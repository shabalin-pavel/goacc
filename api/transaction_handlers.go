package api

import (
	"github.com/gin-gonic/gin"
	"time"
	"goacc/core/utils"
	"net/http"
	"strconv"
	"goacc/core/services"
	"goacc/core/entities"
)

func GetTransactions(ctx *gin.Context) {
	start, end := getDateFilter(ctx)
	if accountId, ok := ctx.GetQuery("source_account"); ok {
		source, _ := strconv.Atoi(accountId)
		ctx.JSON(http.StatusOK, services.GetTransactions(start, end, uint(source)))
		return
	} else {
		ctx.JSON(http.StatusOK, services.GetAllTransactions(start, end))
		return
	}
}

func CreateTransaction(ctx *gin.Context) {
	transaction := &entities.Transaction{}
	if err := ctx.BindJSON(transaction); err == nil {
		transaction.Save()
		ctx.AbortWithStatus(http.StatusOK)
		return
	}
	ctx.AbortWithStatus(http.StatusBadRequest)
	return
}

func getDateFilter(ctx *gin.Context) (startDate, endDate time.Time) {

	if startDateRaw, ok := ctx.GetQuery("start"); ok {
		startDate, _ = time.Parse(time.RFC3339, startDateRaw)
		startDate = utils.TruncateDate(startDate)
	} else {
		startDate = utils.TOTAL_DATE
	}

	if endDateRaw, ok := ctx.GetQuery("end"); ok {
		endDate, _ = time.Parse(time.RFC3339, endDateRaw)
		endDate = utils.TruncateDate(endDate)
	} else {
		endDate = utils.TruncateDate(time.Now().Add(utils.DurationDay(7)))
	}
	return
}
