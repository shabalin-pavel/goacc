import {AppComponent} from './app.component';
import {AccountsComponent} from './accounts/accounts.component';
import {AccountService} from './account.service';
import {AccountEditorComponent} from './account-editor/account-editor.component';
import {AppRoutingModule} from './/app-routing.module';
import {CurrenciesService} from './currencies.service';
import {PlanningViewComponent} from './planning-view/planning-view.component';
import {RecentTransactionsComponent} from './recent-transactions/recent-transactions.component';
import {TransactionEditorComponent} from './transaction-editor/transaction-editor.component';
import {PlanningComponent} from './planning/planning.component';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { TemplatesComponent } from './templates/templates.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountsManagementComponent } from './accounts-management/accounts-management.component';
import {TransactionsService} from './transactions.service';


@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    AccountEditorComponent,
    PlanningViewComponent,
    RecentTransactionsComponent,
    TransactionEditorComponent,
    PlanningComponent,
    TemplatesComponent,
    TransactionsComponent,
    DashboardComponent,
    AccountsManagementComponent,
  ],
  imports: [
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    HttpClient,
    AccountService,
    CurrenciesService,
    TransactionsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
