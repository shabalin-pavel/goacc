import {Account} from './models/Account';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/index';

@Injectable()
export class AccountService {

  constructor(private client: HttpClient) {
  }

  private accounts: BehaviorSubject<Account[]>;

  private accountList: Observable<any>;

  getAccount(id: number): Observable<any> {
    return this.client.get('/api/account/' + id);
  }

  private loadAccountList() {
    this.accountList = this.client.get('/api/accounts_ids');
  }

  getAccountsIdsList() {
    if (this.accountList == null) {
      this.loadAccountList();
    }
    return this.accountList;
  }

  getAccountsList(): Observable<any> {
    return this.client.get('/api/accounts');
  }

  saveAccount(account: Account): Observable<any> {
    return this.client.post('/api/account', JSON.stringify(account));
  }
}
