import {Component, ElementRef, OnInit, Output, ViewChild} from '@angular/core';
import {Transaction} from '../models/Transaction';
import {TransactionsService} from '../transactions.service';
import {AccountService} from '../account.service';
import {Account} from '../models/Account';

declare let $: any;

@Component({
  selector: 'app-transaction-editor',
  templateUrl: './transaction-editor.component.html',
  styleUrls: ['./transaction-editor.component.css']
})
export class TransactionEditorComponent implements OnInit {

  @ViewChild('transactionEditorDialog')
  elementRef: ElementRef;

  @Output()
  transaction: Transaction;

  @Output()
  accountList: Account[];




  constructor(private transactionsService: TransactionsService, private accountService: AccountService) {
  }

  ngOnInit() {
    this.transaction = new Transaction();

    this.accountService.getAccountsList().subscribe((accounts) => {
      this.accountList = accounts;
    });
  }


  edit() {
    this.show();
  }

  show() {
    $(this.elementRef.nativeElement).modal('show');
  }

  templateChanged() {
    alert('Changed!');
  }

  targetChanged() {

  }

  sourceChanged() {

  }
}
