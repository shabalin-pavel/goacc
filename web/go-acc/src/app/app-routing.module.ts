import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlanningComponent} from './planning/planning.component';
import {TemplatesComponent} from './templates/templates.component';
import {AccountsManagementComponent} from './accounts-management/accounts-management.component';
import {TransactionsComponent} from './transactions/transactions.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
  {path: 'planning', component: PlanningComponent},
  {path: 'templates', component: TemplatesComponent},
  {path: 'transactions', component: TransactionsComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'accounts', component: AccountsManagementComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule {
}
