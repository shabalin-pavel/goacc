import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AccountService} from '../account.service';
import {AccountEditorComponent} from '../account-editor/account-editor.component';
import {Account} from '../models/Account';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css'],
})
export class AccountsComponent implements OnInit {

  @Output()
  accounts: Account[];

  @Input()
  @ViewChild(AccountEditorComponent)
  accountEditor: AccountEditorComponent;

  @Output()
  showHidden = false;

  @Output()
  onSelect = new EventEmitter<Account>();

  selectedAccount: Account;

  constructor(private accountService: AccountService, private router: Router) {
  }

  ngOnInit() {
    this.refreshList();
  }

  addAccount() {
    this.accountEditor.show();
  }

  onAdded(saved: boolean) {
    if (saved) {
      this.refreshList();
    }
  }

  private refreshList() {
    this.accountService.getAccountsList().subscribe((accounts: Account[]) => {
      this.accounts = accounts.filter((account: Account) => !account.Hidden || this.showHidden);
      if (this.selectedAccount == null && this.accounts.length > 0) {
        this.selectAccount(this.accounts[0]);
      }
    });
  }

  edit(account: Account) {
    this.accountEditor.edit(account);
  }

  toggleHidden() {
    this.showHidden = !this.showHidden;
    this.refreshList();
  }

  selectAccount(account: Account) {
    this.selectedAccount = account;
    this.onSelect.emit(this.selectedAccount);
  }
}
