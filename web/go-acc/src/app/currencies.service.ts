import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';

@Injectable()
export class CurrenciesService {

  constructor(private client: HttpClient) {
  }

  getCurrencies(): Observable<any> {
    return this.client.get('/api/currencies');
  }
}
