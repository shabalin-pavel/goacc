import {Currency} from './Currency';

export class Account {
  ID: number;
  AccountName: string;
  StarterBalance: number;
  CurrencyID: number;
  Currency: Currency;
  Amount: number;
  Hidden: boolean;
}
