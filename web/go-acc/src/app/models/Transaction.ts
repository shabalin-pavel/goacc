import {Account} from './Account'

export class Transaction {
  ID: number;
  Title: string;
  TransactionDate: Date;
  Amount: number;
  Confirmed: boolean;
  Description: string;

  SourceID: number;
  Source: Account;

  TargetID: number;
  Target: Account;

  TemplateID: number;
  Template: any;

  public getType(): string {
    if (this.Target == null) {
      return 'Payment';
    } else if (this.Source) {
      return 'Account replenishment';
    } else {
      return 'Transfer';
    }
  }

  public getCurrency(): string {
    if (this.Source != null){
      return this.Source.Currency.Code;
    }
    return this.Target.Currency.Code;
  }

}
