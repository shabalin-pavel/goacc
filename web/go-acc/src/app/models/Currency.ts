export class Currency {
  NumberCode: number;
  Code: string;
  Name: string;


  static hasSymbol(currency: Currency): boolean {
    return Currency.getSymbolSign(currency) !== null;
  }

  static getSymbolSign(currency: Currency): string {
    return Currency.signs[currency.Code];
  }

  static signs = {
    'USD': 'dollar',
    'RUB': 'ruble',
  };
}
