import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../models/Account';

@Component({
  selector: 'app-recent-transactions',
  templateUrl: './recent-transactions.component.html',
  styleUrls: ['./recent-transactions.component.css']
})
export class RecentTransactionsComponent implements OnInit {


  @Input()
  account: Account;

  constructor() {
  }

  ngOnInit() {
  }

}
