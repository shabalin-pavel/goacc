import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private client: HttpClient) {
  }

  public getAllTransactions(): Observable<any> {
    return this.client.get('/api/transactions');
  }
}
