import {Component, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TransactionEditorComponent} from '../transaction-editor/transaction-editor.component';
import {TransactionsService} from '../transactions.service';
import {Transaction} from '../models/Transaction';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  @Input()
  @ViewChild(TransactionEditorComponent)
  transactionEditor: TransactionEditorComponent;

  @Output()
  transactionsList: Transaction[];


  constructor(private transactionsService: TransactionsService) {
  }

  ngOnInit() {
    this.transactionsService.getAllTransactions().subscribe((transactions) => {
      this.transactionsList = transactions;
    });
  }

  newTransaction() {
    this.transactionEditor.show();
  }

}
