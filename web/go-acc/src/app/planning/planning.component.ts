import {Component, OnInit, Output} from '@angular/core';
import {Account} from '../models/Account';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {

  @Output()
  selectedAccount: Account;

  constructor() {
  }

  ngOnInit() {
  }

  selectAccount(account: Account) {
    this.selectedAccount = account;
  }
}
