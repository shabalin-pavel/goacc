import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../models/Account';
import {Observable} from 'rxjs/internal/Observable';

@Component({
  selector: 'app-planning-view',
  templateUrl: './planning-view.component.html',
  styleUrls: ['./planning-view.component.css']
})
export class PlanningViewComponent implements OnInit {

  @Input()
  account: Account;

  constructor() {
  }

  ngOnInit() {
  }


  newTransaction() {
    alert('New transaction');
  }
}
