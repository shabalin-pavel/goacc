import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AccountService} from '../account.service';
import {Account} from '../models/Account';
import {Currency} from '../models/Currency';
import {CurrenciesService} from '../currencies.service';

declare let $: any;


@Component({
  selector: 'app-account-editor',
  templateUrl: './account-editor.component.html',
  styleUrls: ['./account-editor.component.css']
})
export class AccountEditorComponent implements OnInit {

  constructor(private accountService: AccountService, private currenciesService: CurrenciesService) {
  }

  @Output()
  account: Account = new Account();

  @Output()
  currencies: Currency[];

  @ViewChild('accountEditorDialog')
  elementRef: ElementRef;

  @Output()
  saved: EventEmitter<boolean> = new EventEmitter();

  currencyCode: string;

  editMode: boolean;

  ngOnInit() {
    this.currenciesService.getCurrencies().subscribe((currencies: Currency[]) => {
      this.currencies = currencies;
    });
  }

  show() {
    $(this.elementRef.nativeElement).modal('show');
  }

  currencyCodeChanged() {
    this.account.CurrencyID = parseInt(this.currencyCode);
  }

  save() {
    alert(JSON.stringify(this.account));
    this.accountService.saveAccount(this.account).subscribe((account: Account) => {
      this.saved.emit(true);
    });
  }

  create() {
    this.editMode = false;
    this.show();
  }

  edit(account: Account) {
    this.account = account;
    this.currencyCode = this.account.CurrencyID.toString();
    this.editMode = true;
    this.show();
  }
}
